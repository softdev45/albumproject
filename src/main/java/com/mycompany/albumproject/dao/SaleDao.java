/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.dao;

import com.mycompany.albumproject.helper.DatabaseHelper;
import com.mycompany.albumproject.model.ReportSale;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author bwstx
 */
public class SaleDao {

    public List<ReportSale> getDayReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%M-%d\", InvoiceDate) as period, sum(Total) as total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReportSale> getMonthReport() {
        ArrayList<ReportSale> list = new ArrayList();
        String sql = "SELECT strftime(\"%Y-%M\", InvoiceDate) as period, sum(Total) as total FROM invoices\n"
                + "LEFT JOIN invoice_items\n"
                + "ON invoices.InvoiceId = invoice_items.InvoiceId\n"
                + "GROUP BY period\n"
                + "ORDER BY period DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReportSale item = ReportSale.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}

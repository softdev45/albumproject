/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.service;

import com.mycompany.albumproject.dao.SaleDao;
import com.mycompany.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author bwstx
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    public List<ReportSale> getReportSaleByMonth() {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport();
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.albumproject.poc;

import com.mycompany.albumproject.model.ReportSale;
import com.mycompany.albumproject.service.ReportService;
import java.util.List;

/**
 *
 * @author bwstx
 */
public class TestReportSale {
    public static void main(String[] args) {
        ReportService reportService = new ReportService();
//        List<ReportSale> report = reportService.getReportSaleByDay();
//        for(ReportSale r:report){
//             System.out.println(r);
//        }
        List<ReportSale> reportMonth = reportService.getReportSaleByMonth();
        for(ReportSale r:reportMonth){
             System.out.println(r);
        }
    }
}
